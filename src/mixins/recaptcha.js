export default {
    data() {
        return {
            $_recaptcha_siteKey: "",
            $_recaptcha_token: "",
            $_recaptcha_callback: null
        }
    },
    created()
    {
        this.$_recaptcha_siteKey = process.env.VUE_APP_RECAPTCHA_SITE_KEY;
    },
    methods: {
        /**
         * Executar ação relacionada ao captcha.
         */
        $_recaptcha_verify(callback)
        {
            this.$_recaptcha_callback = callback || null;
            this.$refs.recaptcha && this.$refs.recaptcha.execute();
        },

        $_recaptcha_onVerified(recaptchaToken)
        {
            this.$_recaptcha_token = recaptchaToken;
            this.$_recaptcha_callback && this.$_recaptcha_callback();
        },

        $_recaptcha_onExpired()
        {
            this.$refs.recaptcha && this.$refs.recaptcha.reset();
        }
    }
}