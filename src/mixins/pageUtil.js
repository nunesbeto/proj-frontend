function _findIndex(data, fields, values)
{
    return data.findIndex(el => {
        let match = true;
        fields.forEach(field => {
            if (match && el[field] !== values[field]) {
                match = false;
            }
        })
        return match;
    });
}

export default {
    data() {
        return {
            itemToRemove: null
        }
    },
    methods: {
        /**
         * Exibir modal de confirmação de exclusão de um item.
         *
         * @param {Object}         item  Item a ser removido.
         * @param {Element|string} modal Modal a ser exibido.
         */
        showRemoveConfirm(item, modal)
        {
            this.itemToRemove = Object.assign({}, item);

            if ((typeof modal === "string" || modal instanceof String) && this.$refs[modal]) {
                modal = this.$refs[modal];
            }

            if (typeof modal.show === "function") {
                modal.show();
            }
        },

        /**
         * @param {Object[]} data
         * @param {string[]} fieldsToCompare
         */
        removeItemFromData(data, fieldsToCompare)
        {
            let valuesToCompare = {};

            fieldsToCompare.forEach(field => {
                valuesToCompare[field] = this.itemToRemove[field]
            })

            let idx = _findIndex(data, fieldsToCompare, valuesToCompare);

            // data[idx]["isRemoving"] = true;
            this.$set(data[idx], "isRemoving", true);

            setTimeout(() => {
                idx = _findIndex(data, fieldsToCompare, valuesToCompare);
                data.splice(idx, 1);
            }, 3000);
        }
    }
}