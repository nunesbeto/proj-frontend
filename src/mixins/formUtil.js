export default {
    data() {
        return {
            formErrors: {},
        }
    },
    methods: {
        /**
         * Limpar erros de formulários.
         */
        clearFormErrors()
        {
            this.formErrors = {};
        }
    }
}