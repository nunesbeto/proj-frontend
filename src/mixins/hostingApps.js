export default {
    data() {
        return {
            apps: [
                { id: 26,    title: "Wordpress",   category: "CMS",         name: "wordpress" },
                { id: 10002, title: "Admin Host",  category: "Revenda",     name: "adminhost" },
                { id: 413,   title: "Joomla",      category: "CMS",         name: "joomla" },
                { id: 543,   title: "Drupal",      category: "CMS",         name: "drupal" },
                { id: 688,   title: "Magento",     category: "E-commerce",  name: "magento" },
                { id: 591,   title: "Prestashop",  category: "E-commerce",  name: "prestashop" },
                { id: 499,   title: "Opencart",    category: "E-commerce",  name: "opencart" },
                { id: 144,   title: "WHMCS",       category: "E-commerce",  name: "whmcs" },
                { id: 514,   title: "Mautic",      category: "Marketing",   name: "mautic" },
                { id: 421,   title: "MediaWiki",   category: "Wiki",        name: "mediawiki" },
                { id: 2,     title: "phpBB",       category: "Fórum",       name: "phpbb" },
                { id: 542,   title: "Moodle",      category: "Educacional", name: "moodle" },
                { id: 419,   title: "Laravel",     category: "Framework",   name: "laravel" },
                { id: 123,   title: "CodeIgniter", category: "Framework",   name: "codeigniter" },
            ],
        }
    }
}
