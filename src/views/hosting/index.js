import HostingCreate        from "./HostingCreate"
import HostingMigration     from "./HostingMigration"
import HostingWordpress     from "./HostingWordpress"
import HostingCharts        from "./HostingCharts"
import HostingSummary       from "./HostingSummary"
import HostingDomain        from "./HostingDomain"
import HostingDomainEdit    from "./HostingDomainEdit"
import HostingDns           from "./HostingDns"
import HostingAlias         from "./HostingAlias"
import HostingSubdomain     from "./HostingSubdomain"
import HostingRedirect      from "./HostingRedirect"
import HostingFiles         from "./HostingFiles"
import HostingFtp           from "./HostingFtp"
import HostingDeploy        from "./HostingDeploy"
import HostingBackup        from "./HostingBackup"
import HostingDatabase      from "./HostingDatabase"
import HostingEmail         from "./HostingEmail"
import HostingEmailSettings from "./HostingEmailSettings"
import HostingEmailAccess   from "./HostingEmailAccess"
import HostingAntispam      from "./HostingAntispam"
import HostingWebmail       from "./HostingWebmail"
import HostingApps          from "./HostingApps"
import HostingAppInstall    from "./HostingAppInstall"
import HostingPhp           from "./HostingPhp"
import HostingSsl           from "./HostingSsl"
import HostingSsh           from "./HostingSsh"
import HostingCron          from "./HostingCron"
import HostingLogs          from "./HostingLogs"
import HostingPlan          from "./HostingPlan"
import HostingTransfer      from "./HostingTransfer"
import HostingTransferConfirmation from "./HostingTransferConfirmation"
import HostingBalancer      from "./HostingBalancer"
import HostingRemove        from "./HostingRemove"

export {
    HostingCreate,
    HostingMigration,
    HostingWordpress,
    HostingCharts,
    HostingSummary,
    HostingDomain,
    HostingDomainEdit,
    HostingDns,
    HostingAlias,
    HostingSubdomain,
    HostingRedirect,
    HostingFiles,
    HostingFtp,
    HostingDeploy,
    HostingBackup,
    HostingDatabase,
    HostingEmail,
    HostingEmailSettings,
    HostingEmailAccess,
    HostingAntispam,
    HostingWebmail,
    HostingApps,
    HostingAppInstall,
    HostingPhp,
    HostingSsl,
    HostingSsh,
    HostingCron,
    HostingLogs,
    HostingPlan,
    HostingTransfer,
    HostingTransferConfirmation,
    HostingBalancer,
    HostingRemove
}