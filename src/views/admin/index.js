import AdminWelcome    from './AdminWelcome'
import AdminFirstSteps from './AdminFirstSteps'
import AdminDashboard  from './AdminDashboard'
import AdminHostings   from './AdminHostings'
import AdminDomains    from './AdminDomains'
import AdminSettings   from './AdminSettings'

export {
    AdminWelcome,
    AdminFirstSteps,
    AdminDashboard,
    AdminHostings,
    AdminDomains,
    AdminSettings
}