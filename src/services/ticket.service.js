import { Api } from '../plugins/api'

/**
 * Carregar dados de um ticket de suporte.
 *
 * @param {string|number} id  ID do ticket de suporte
 * @return {Object}           Dados do ticket de suporte
 */
function get(id)
{
    return Api.get(`/ticket/${id}`).then((response) => response.data.data);
}

export default {
    get
}