<?php

namespace Deployer;

require 'vendor/deployer/deployer/recipe/common.php';
require 'vendor/deployer/recipes/recipe/npm.php';

// Project name
set('application', 'app-client');

// Project repository
set('repository', 'git@gitlab.com:hostoo/app-client.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Shared dirs/files between deploys
set('shared_dirs', []);
set('shared_files', ['.env']);

// Writable dirs by web server 
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts
set('default_stage', 'staging');

host('beta.hostoo.io')
    ->stage('staging')
    ->hostname('hostoo-cloud-app-deploy')
    ->set('deploy_path', '/var/www/beta.hostoo.io')
    ->set('http_user', 'admin')
    ->multiplexing(false);

host('app.hostoo.io')
    ->stage('production')
    ->hostname('hostoo-cloud-app-deploy')
    ->set('deploy_path', '/var/www/app.hostoo.io/front')
    ->set('http_user', 'admin')
    ->multiplexing(false);

// Tasks
desc('Execute npm run build');
task('npm:production', '{{bin/npm}} run build');

desc('Update package.json with version from Git');
task('npm:version', '{{bin/git}} describe | xargs -I {} {{bin/npm}} --allow-same-version --no-git-tag-version version {}');

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'npm:version',
    'npm:install',
    'npm:production',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
