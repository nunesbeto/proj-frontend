# Changelog

## 2.29.11 (2023-08-18)

### Alterado

- Ajuste visual no banner da campanha de indicações

## 2.29.10 (2023-08-16)

### Adicionado

- Campanha de indicações de agosto/2023

## 2.29.8 (2023-07-14)

### Alterado

- Altera a ordem do aplicativo de revenda

## 2.29.7 (2023-07-12)

### Corrigido

- Corrige mensagem sobre término de créditos

## 2.29.5 (2023-07-11)

### Adicionado

- Instalador do novo aplicativo de revenda

## 2.29.4 (2023-07-11)

### Alterado

- Ajusta mensagem na lista de indicações do afiliado
- Ajusta idioma no header do painel
- Ajuste nos campos do formulário de recarga

## 2.29.1 (2023-06-09)

### Adicionado

- Adiciona alerta sobre período de teste
- Adiciona tooltip informando valor de créditos e bônus

## 2.29.0 (2023-06-01)

### Adicionado

- Permissão para utilizar o mesmo telefone em mais de uma conta

## 2.28.1 (2023-05-31)

### Alterado

- Adiciona session_id ao evento de compra do GA4

## 2.28.0 (2023-05-31)

### Adicionado

- Transferência de hospedagem entre clientes

### Alterado

- Ajuste em exibição de dados na tela de login

### Corrigido

- Ajusta exclusão de cookie com token de acesso no logout
- Ajusta redirecionamento de URL no login

## 2.27.0 (2023-05-25)

### Adicionado

- Alerta sobre término de créditos

### Alterado

- Tela de login
- Recarga por período precisa ter 1 serviço ativo
- Limita a lista de indicações do afiliado

## 2.26.16 (2023-05-17)

### Removido

- Remove campanha de Indicações

## 2.26.15 (2023-05-16)

### Adicionado

- Campanha para usuários em período de teste

### Corrigido

- Ajuste no menu da tela de hospedagem

## 2.26.14 (2023-04-27)

### Adicionado

- Envia eventos de ecommerce do Google Analytics 4

## 2.26.13 (2023-04-26)

### Adicionado

- Envia eventos do Google Analytics 4

## 2.26.7 (2023-04-05)

### Alterado

- Destaque no valor de renovação do domínio ao registrar
- Altera texto do botão de abrir ticket

## 2.26.6 (2023-03-23)

### Corrigido

- Corrige exibição do banner para campanha de Indicações

## 2.26.5 (2023-03-23)

### Adicionado

- Banner para campanha de Indicações

### Corrigido

- Corrige exibição do saldo atual

## 2.26.4 (2023-03-23)

### Adicionado

- Rastreamento de cliques no banner de Mês do Consumidor
- Rastreamento de cliques no botão de indicações
- Rastreamento de cliques nos botões de recarga manual

### Alterado

- Ajusta indicações para campanha de março/2023
- Ajusta validação do valor máximo de recarga
- Ajusta exibição de períodos na recarga manual
- Melhora exibição de link para recarga manual

## 2.26.3 (2023-03-15)

### Adicionado

- Adiciona períodos de recarga de 24 e 36 meses

### Alterado

- Ajusta promoção do cliente para Mês do Consumidor

## 2.26.2 (2023-03-02)

### Adicionado

- Carregar e-mail do usuário na tela de verificação de telefone

## 2.26.1 (2023-02-10)

### Corrigido

- Corrige undefined no gerador de senha

### Removido

- Remove chamada do Jivochat em ambiente local

## 2.26.0 (2023-02-01)

### Adicionado

- Recarga manual por período

## 2.25.1 (2022-12-15)

### Adicionado

- Alerta sobre tempo de propagação para a tela de suspensão

### Alterado

- Ajuste na exibição do título do questionário

## 2.25.0 (2022-12-14)

### Adicionado

- Disparos de mensagens automáticas no JivoChat
- Configuração da tela de suspensão personalizada

### Corrigido

- Corrige redirecionamento no registro de domínio
- Ajustes na autenticação de 2 fatores
- Ajusta URL da base de conhecimento
- Corrige botão de fechar questionário
- Ajusta load infinito na busca pré-ticket
- Corrige problema em exibição de respostas no questionário

## 2.24.0 (2022-11-17)

### Alterado

- Priorização da recarga automática
- Ajusta mensagem de estimativa de créditos

## 2.23.4 (2022-11-14)

### Corrigido

- Corrige links para Termos de Uso

## 2.23.3 (2022-11-11)

### Adicionado

- Banner para promoção de Black Friday

### Corrigido

- Limita tamanho da senha do usuário a 100 caracteres

## 2.23.1 (2022-10-18)

## 2.23.0 (2022-10-18)

### Adicionado

- Tela de migração para usuário logado
- Tela de gerenciamento de instalações WordPress
- Tela de primeiros passos

### Alterado

- Ajusta botões em destaque no Acesso Rápido

### Corrigido

- Corrige proteção contra XSS no redirecionamento

## 2.22.1 (2022-10-07)

### Adicionado

- Busca e sugestão de artigos na base de conhecimento

## 2.22.0 (2022-10-07)

### Adicionado

- Redirecionamento para HTTPS
- Módulo de NPS e questionários
- Proteção contra XSS no redirecionamento após login

## 2.21.1 (2022-08-15)

## 2.21.0 (2022-08-04)

## 2.20.0 (2022-07-10)

## 2.19.3 (2022-07-07)

## 2.19.2 (2022-06-22)

### Alterado

- Altera versão do Magento para 2.4.2 (suporte a MySQL 5.7 e 8.0)

## 2.19.1 (2022-06-21)

### Alterado

- Altera versão do Magento para 2.4

## 2.19.0 (2022-06-10)

## 2.18.5 (2022-04-12)

### Corrigido

- Ajuste em rota do endpoint de carregar arquivo

## 2.18.4 (2022-04-06)

### Alterado

- Ajuste em tela de login do webmail

## 2.18.3 (2022-03-17)

### Adicionado

- Tela para solicitação de participação no Programa de Afiliados
- Aviso sobre ativaçao do acelerador

### Corrigido

- Correção em valor mínimo de recarga
- Imagem de loading para botões do tipo link

## 2.18.2 (2022-02-15)

### Adicionado

- Tela para solicitação de migração de hospedagem

## 2.18.1 (2022-02-09)

### Adicionado

- Código do Google Tag Manager

### Removido

- Remoção do código do Google Analytics

## 2.18.0 (2022-02-02)

### Adicionado

- Recarga automática via boleto bancário

### Removido

- Remoção de função para upgrade de webmail
- Remoção de função para gerar certificado SSL do webmail

## 2.17.1 (2021-12-08)

### Alterado

- Ajuste nas funções do Balanceador
- Mudança no valor mínimo de recarga

### Removido

- Aviso sobre ajuste de preços

## 2.17.0 (2021-10-27)

### Adicionado

- Aviso sobre ajuste de preços em novembro de 2011
- Tela de balanceador de hospedagens

### Corrigido

- Correção em card com lista de tickets

## 2.16.2 (2021-10-06)

### Corrigido

- Correção em Editor de arquivos

## 2.16.1 (2021-10-04)

### Corrigido

- Correção em autenticação de 2 fatores

## 2.16.0 (2021-10-01)

### Adicionado

- Autenticação em 2 fatores
- Adiciona campo de busca no Editor de arquivos
- Adiciona botão de ajuda do Gerenciador
- Mensagem de notificação ao habilitar o certificado SSL do webmail

### Removido

- Remove checkbox de Manter conectado na tela de login

## 2.15.0 (2021-08-25)

### Adicionado

- Gráfico de histórico de conexões ao banco de dados
- Adiciona filtro de servidor em lista de hospedagens
- Adiciona botão de atualizar gráfico de consumo
- Adiciona mensagem de alerta sobre o certificado SSL do webmail

### Alterado

- Altera cor do cache no gráfico de consumo de RAM

## 2.14.0 (2021-08-11)

### Adicionado

- Adicionar tutoriais em vídeo
- Exibir informação de memória em cache nos gráficos de consumo

### Corrigido

- Correção em mensagem informativa sobre recarga automática
- Correção em exibição do uso de disco na hospedagem

## 2.13.2 (2021-07-28)

### Corrigido

- Ajustes em eventos do Facebook nas etapas de cadastro
- Ajuste em textos do Programa de Indicação

## 2.13.0 (2021-07-19)

### Adicionado

- Programa de indicação de clientes

## 2.12.0 (2021-06-22)

### Adicionado

- Definir região na criação de hospedagem
- Exibir região da hospedagem

### Alterado

- Ajustes para considerar o preço da hospedagem e dos planos por região

### Corrigido

- Correção no gerenciador de arquivos para selecionar múltiplos arquivos no MacOS

## 2.11.0 (2021-06-15)

### Adicionado

- Aviso no gerenciador de arquivos sobre versão para celular
- Recarga com PIX

### Alterado

- Mudança de layout no pop-up para ativação do Acelerador de WordPress
- Ajuste em verificação de crédito emergencial

### Corrigido

- Correção na ativação do acelerador de sites
- Correção em campos de endereço na recargac

### Removido

- Remoção do botão para download do banco de dados

## 2.10.0 (2021-05-31)

### Adicionado

- Exibir campos de endereço na recarga via PicPay
- Campo de número (endereço) na edição de perfil do usuário

### Corrigido

- Correção em fingerprint na recarga com cartão de crédito
- Ocultar função de alterar nameservers quando o domínio estiver pendente

## 2.9.1 (2021-05-13)

### Adicionado

- Uso de fingerprint em pagamentos via cartão de crédito
- Recarga vinculada a um registro ou transferência de domínio

### Alterado

- Compactar informações na aba Resumo da hospedagem
- Habilitar opção SameSite no cookie com token de acesso

### Corrigido

- Correção em função de logout
- Ajuste em telas com funções de gerenciamento de domínio

## 2.9.0 (2021-05-11)

### Adicionado

- Função para atualizar webmail para nova versão
- Função para ativar domínio alternativo após a instalação de um aplicativo
- Tela para pesquisa de domínios e sugestões de domínios semelhantes
- Permitir registro de domínio após login ou cadastro
- Logout no servidor

### Alterado

- Ajuste no alerta de notificação de erro em caso de erro 502
- Ajuste visual em tela de criação de hospedagem

## 2.8.2 (skipped)

## 2.8.1 (2021-05-06)

### Corrigido

- Correção em botão com link para o webmail

## 2.8.0 (2021-05-05)

### Adicionado

- Funções de personalização para o webmail
- Gerar certificado SSL para o webmail manualmente
- Campo de CNPJ na recarga com cartão de crédito

### Alterado

- Melhoria na consulta para obter dados sobre e-mails

### Corrigido

- Correção no logotipo da Hostoo nas telas de login e cadastro

## 2.7.3 (skipped)

## 2.7.2 (2021-04-09)

### Alterado

- Alteração de texto na tela de restauração de backup

## 2.7.1 (2021-04-07)

### Adicionado

- Renovação de domínio

## 2.7.0 (2021-03-23)

### Adicionado

- Opções para customização da restauração de backup
- Modal para feedback durante a remoção da última hospedagem
- Aviso sobre ativaçao da recarga automática

### Alterado

- Notificações de consumo desabilitadas por padrão
- Mudança em mensagem introdutória na tela de SSL

### Corrigido

- Correção no acesso com rede social no caso de falha de permissão

## 2.6.5 (2021-03-02)

### Corrigido

- Ajuste na função de verificar instalação de WordPress na hospedagem

## 2.6.4 (skipped)

## 2.6.3 (skipped)

## 2.6.2 (skipped)

## 2.6.1 (2021-02-08)

### Alterado

- Executar funções do acelerador e instalação de plugin de cache em segundo plano

## 2.6.0 (2021-02-08)

### Adicionado

- Gerenciamento de domínios
- Adicionar os campos serviço e protocolo na criação de registros SRV

### Alterado

- Mudança no ID do aplicativo PrestaShop 

### Corrigido

- Correção de texto em mensagem do tutorial de criação de hospedagem
- Correção na função de autocompletar endereço no formulário de recarga automática

### Removido

- Recarga via PagSeguro

## 2.5.0 (2020-11-12)

### Adicionado

- Acelerador de WordPress
- Instalação de plugin de cache da Hostoo em site WordPress

### Alterado

- Bloquear campos de cidade e estado após inserir um CEP válido

### Corrigido

- Correção na tela de excluir hospedagem

## 2.4.2 (2020-10-14)

### Corrigido

- Na lista de tickets de suporte, não permitir clique no domínio caso este já tenha sido excluído
- Desabilitar eventos do teclado ao abrir o modal de compactar arquivos

## 2.4.1 (2020-10-06)

### Alterado

- Permitir recarga mínima de 7 reais para todas as formas de pagamento exceto boleto
- Ajuste na mensagem de erro para o upload de arquivos maiores que 100 MB
- Exibir mensagem de erro sobre o valor mínimo de recarga

## 2.4.0 (2020-09-29)

### Adicionado

- Exibir campos de endereço na recarga via boleto

### Corrigido

- Correção no filtro da tabela na tela de Bancos de Dados

### Removido

- Remoção de funções do PagSeguro no pagamento com cartão de crédito

## 2.3.3 (2020-09-16)

### Alterado

- Ajuste no alerta de notificação de erro em caso de erro 503 (manutenção no servidor)

## 2.3.2 (2020-08-26)

### Corrigido

- Correção na tela de Notificações
- Correção no acesso com rede social cujo e-mail já está sendo usado em outra conta

## 2.3.1 (2020-08-24)

### Corrigido

- Correções nos eventos do pixel do Facebook
- Correção na mensagem de requisito recomendado para os aplicativos

## 2.3.0 (2020-08-24)

### Adicionado

- Recarga automática com cartão de crédito
- Informação dos arquivos de logs de erro do PHP
- Informação do requisito recomendado de memória RAM para Moodle, Mautic e Magento

### Removido

- Removida a opção de visualizar logs de erro do Wordpress

## 2.2.3 (2020-07-16)

### Alterado

- Ignorar erro ao obter dados de tickets de suporte recentes
- Ajuste no alerta de notificação de erro em caso de erro 522 do Cloudflare

## 2.2.2 (2020-07-15)

### Alterado

- Executar função de alterar configurações do PHP em segundo plano

## 2.2.1 (2020-07-14)

### Adicionado

- Copiar valor do subdomínio para o campo Diretório no alerta de adicionar subdomínio

### Corrigido

- Correção em texto do alerta de confirmação na tela de Backup

## 2.2.0 (2020-07-13)

### Adicionado

- Registros DNS do tipo AAAA
- Campo de nome ao criar registros DNS do tipo MX

### Alterado

- Executar função de ativar domínio alternativo em segundo plano
- Exibir opção de enviar notificação de erro apenas quando for um erro do servidor
- Ocultar campo TTL para registros DNS do tipo MX
- Aba Resumo renomeada para Home
- Opção de finalizar no tutorial de criação de hospedagem
- Alteração em texto descritivo da tela de Backup
- Executar função de extrair arquivos em segundo plano

### Corrigido

- Ajustes responsivos
- Correção no texto do ticket de notificação de erro
- Ajustes responsivos
- Ocultar botão de alterar plano em hospedagens compartilhadas
- Ajustes no alerta de notificação de erro
- Correções no tutorial de criação de hospedagem
- Ajustes na tela de visualização do ticket de suporte
- Correção na função de renomear arquivo

## 2.1.0 (2020-05-05)

### Adicionado

- Função de remoção de hospedagem em segundo plano
- Função de instalação de aplicativos em segundo plano

## 2.0.4 (2020-04-29)

### Adicionado

- Informações de stack trace no ticket de notificação de erro

### Alterado

- Deixar selecionável toda a caixa de alterar plano
- Ocultar dados de requisição à API no ticket de notificação de erro

### Corrigido

- Correção no botão de solicitar saque de afiliado

## 2.0.3 (2020-04-20)

### Adicionado

- Função de alterar o limite de espaço de uma conta de e-mail
- Informação de como criar tarefas cron para comandos PHP
- Informação do domínio no ticket de Notificação de erro

### Corrigido

- Correção no cancelamento de requisições ajax ao mudar de página
- Correção no logotipo da Hostoo usado no cabeçalho de e-mails
- Correção na visualização de logs do deploy
- Correção na visualização da mensagem do ticket de suporte

## 2.0.2 (2020-04-14)

### Adicionado

- Duração estimada na recarga de créditos
- Desabilitar cache de arquivos HTML

### Corrigido

- Correção no carregamento das informações do plano ao mudar de hospedagem
- Correção na exibição de crédito negativo na barra do topo
- Correção na exibição do alerta de erro

## 2.0.1 (2020-04-10)

### Adicionado

- Arquivos CSS do webmail
- Favicons e outras imagens públicas necessárias

### Corrigido

- Correção em redirecionamento de rotas do painel antigo

### Removido

- Remoção de alertas com mensagem sobre versão beta

## 2.0.0 (2020-04-09)

- Não documentado (primeira versão do CHANGELOG)
- Por se tratar da segunda versão do painel da Hostoo, escolheu-se começar o versionamento em 2.0.0

[comment]: <> (As versões podem ter as seções Adicionado, Alterado, Corrigido e Removido)
