const package = require('./package.json')
const webpack = require('webpack')
const fs      = require('fs')
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts')

process.env.VUE_APP_SENTRY_RELEASE = package.name + '@' + package.version;

module.exports = {
    lintOnSave: false,
    runtimeCompiler: true,
    configureWebpack: {
        entry: {
            bootstrap: [
                './src/sass/bootstrap.scss',
            ],
            app: [
                './src/stylus/app.styl',
            ],
        },
        resolve: {
            alias: {
                // 'jquery': 'jquery/dist/jquery.slim.js',
                'bootstrap-vue$': 'bootstrap-vue/src/index.js'
            }
        },
        plugins: [
            // new webpack.ProvidePlugin({
            //     '$': 'jquery',
            //     jQuery: 'jquery',
            // }),
            new webpack.IgnorePlugin({
                resourceRegExp: /^\.\/locale$/,
                contextRegExp: /moment$/
            }),
            new RemoveEmptyScriptsPlugin({
                verbose: false
            }),
        ],
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        name: 'vendor',
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10,
                        chunks: 'initial'
                    }
                }
            }
        }
    },
    chainWebpack: config => {
        config.module
            .rule('js')
                .exclude
                    .add(/node_modules\/(?!bootstrap-vue\/src\/)/);

        config
            .plugin('html')
            .tap(args => {
                args[0].chunksSortMode = function(a, b) {
                    const order = ["bootstrap", "auth", "app", "support"];
                    return order.indexOf(a) - order.indexOf(b);
                };
                return args;
            });

        config
            .plugin('copy')
            .tap(args => {
                args[0].patterns.push({
                    from: './node_modules/@fortawesome/fontawesome-free/webfonts',
                    to: './fonts',
                    toType: 'dir',
                });
                return args;
            });
    },
    css: {
        extract: true,
        loaderOptions: {
            stylus: {
                use: [
                    require('nib')()
                ],
                import: [
                    '~@/stylus/variables.styl'
                ]
            }
        }
    },
    devServer: {
        server: {
            type: 'https',
            options: {
                key: fs.readFileSync('./docs/ssl/private.key'),
                cert: fs.readFileSync('./docs/ssl/private.pem'),
            }
        }
    }
}
