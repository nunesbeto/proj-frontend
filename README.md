# app-client

## Project setup
```bash
npm install
```

### Compiles and hot-reloads for development
```bash
npm run serve
```

### Compiles and minifies for production
```bash
npm run build
```

### Run your tests
```bash
npm run test
```

### Lints and fixes files
```bash
npm run lint
```

### Deploy

Exemplo de deploy em um servidor de produção:
```bash
npm version patch
git push origin master --tags
dep deploy --log=deploy.log --hosts=app.hostoo.io --tag=v2.1.0
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
