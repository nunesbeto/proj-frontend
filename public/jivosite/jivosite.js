function jivo_onLoadCallback()
{
    // Avoid creating the Jivo widget
    window.jivo_cstm_widget = document.createElement('div');
    jivo_cstm_widget.setAttribute('id', 'jivo_custom_widget');
    document.body.appendChild(jivo_cstm_widget);

    document.dispatchEvent(new CustomEvent('jivoLoaded'));
}

function jivo_onOpen() {
    document.dispatchEvent(new CustomEvent('jivoOpened'));
}

function jivo_onClose() {
    document.dispatchEvent(new CustomEvent('jivoClosed'));
}

function jivo_onMessageSent() {
    document.dispatchEvent(new CustomEvent('jivoFirstMessageSent'));
}

function jivo_onMessageReceived() {
    document.dispatchEvent(new CustomEvent('jivoMessageReceived'));
}
